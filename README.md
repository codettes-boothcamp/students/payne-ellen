# About me

Hi,I am Payne Ellen.I was born on the 5th of june 1983.So i'm 36 years old.I'm from Suriname and comes from a very big family.
I do not want to have a big family of my own.I have 2 handsome boys and hopefully i will go for a third child if i have a stable
live but i know and believe in A Mighty God and with Him everything is possible.I went back to finish my high school(night school.)
Class 2018/2019.I'm proud of myself but prouder of my son Shamil cause he was the biggest push behind me.So today i'm here again
with a different challenge in my life.If you look in the ICT world you see more men then women.I was asking myself if there are
women out there who can also make a different in the ICT world.Because i know that if we put our mind to do something we can get
it done.Therefore i'm here to make a different but to also be an example for my kids and for women in Suriname.So if i can do it,i know you
can do it to.I like to say this to my sons everytime when they say; I can't do this or I can't do it.Then i say; Never say  that
you can't do this or you can't do it,if you never tried.Oh my hobbies are singing,try something different,swimming etc.
I love to sing more in church,when i'm sad or when i'm nervous.Singing keeps me calm.Yes that's me.With Love...

# Final project








---
TEST
Example [MkDocs] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:alpine

before_script:
  - pip install mkdocs
  ## Add your custom theme if not inside a theme_dir
  ## (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
  # - pip install mkdocs-material

pages:
  script:
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at MkDocs [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `mkdocs.yml`, from `"https://pages.gitlab.io/mkdocs/"` to
`site_url = "https://namespace.gitlab.io"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
[install]: http://www.mkdocs.org/#installation
[documentation]: http://www.mkdocs.org
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

---

Forked from https://gitlab.com/morph027/mkdocs
