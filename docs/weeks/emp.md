 My third Week

**EMBEDDED PROGRAMMING**

Assignment

- Practicing how to use the tools we get in the Arduino Uno kit
- Installing Arduino 1.8.9 for windows
- Programming basics in the Arduino IDE

**Learning outcomes**

- Working with The Arduino Uno Board
  connecting it with our Laptop using a USB cable
  and giving the commands by codiing it in the
  Arduino IDE
- Knowing the Breadboard and all other
  components in the Arduino Kit and checking its
  specs online if needed by the Serial
  Number
- Understanding the Electrical Current Flow

**Arduino Uno**

We started with the Arduino Starter Kit which
included: - The Arduino Uno Board
          - Breadboard
          - USB cable
          - LED's
          - Jumperwires
          - Resistors
          - Button
          - Capacitors
          - H-bridge
          - Transistors ect
          
 **This is the Arduino Uno**
          
 ![tag](../img/1.jpeg)

 At first you connect the Arduino Uno with the USB cable and second you connect it to your pc.
 
 ![tag](../img/3.jpeg)
 
**Resistor**

![tag](../img/res.png)

 A resistor is a passive two-terminal electrical resistance as a
 circuit element.In electronics circuits,resistors are used to
 reduce current flow,adjust signal levels,to divine voltages,
 be as active elements and terminate transmission lines among
 other uses.
 
 **Resistor color code table**
 
 Every resistor have various colors and all these
 colors has meanings as shown in the Resistance color code
 table that follows.
 
 ![tag](../img/2.jpeg)

 After installing the Arduino IDE,we learn how to set up our
 first blink,the yellow light on the Arduino Uno.

 ![tag](../img/1stblink.png)
 
 We first select the COM3 Port for the Arduino/Genuino Uno
 by going to Tools-->Port"COM3."

 ![tag](../img/port.png)
 
 We also select the Arduino Uno board before working by going
 to Tools-->Board--Arduino Uno
 
 ![tag](../img/ardu.png)
 
 To Blink we use the Blink example by going to File-Examples-Basics-Blink.
 
 ![tag](../img/10.png)
 
 Spaceship interface.(blink led 13 with a delay of 1 sec.)

 ![tag](../img/blink1.png)
 
 **A code to blink build in led 13**

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second

   digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}

```

![tag](../img/4thblink.png)

![tag](../img/2ndblink.png)

**Build a Basic Blink circuit with 1 led**

+ First you wire up your board to Arduino's 5V and ground connection(using red and black jumper wires)
+ Next you place a led on the breadboard
+ Then you connect the cathode(short leg)of the led to ground by using a 220 ohm resistor
+ Add a black jumper wire to connect the led with the Arduino (No Code->Led On)

![tag](../img/5thblink.png)

![tag](../img/5th1blink.png)

![tag](../img/3thblink.png)

**Build A Basic circuit to Blink**

To define a pin you have to go to void setup and write
pinMode(whatever pin you want to define, input or output)
What you put in the setup only runs once.
We used three leds and they were defined in pinmode 13,12 and 11.

In the loop the code runs "forever".
With digitalWrite you're explaining when you want the leds on and off (you use high,low or 0,1).
With the delay you can set how long you want the led to stay off.
With this code we want the leds to blink together so they're either all on or all off.
You can change the delay time so the leds will blink faster or slower.
But first you have to code it and opload it to let it blink.

+ First you wire up your board to Arduino's 5V and ground connection
+ Next you place 3 leds on the breadboard
+ Then you connect the cathode(short leg)of each leds to ground by using a 220 ohm resistor
+ At last you connect the anode(long leg)one to pin 11,one to pin 12 and the last one to pin 13

**The code for this blink**
```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(11, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(11, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(11, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second

}

```

![tag](../img/6th1blink.png)

![tag](../img/6thblink.png)

**The Spaceship Interface**

**And this is the code**

```
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
  
}

void ledsOff(){
    digitalWrite(3,LOW);
    digitalWrite(3,LOW);
    digitalWrite(3,LOW);
}

// the loop function runs over and over again forever
void loop() {
  switchState = digitalRead(2);
  // this is a comment
  if (switchState==LOW){
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  }
  else{// wait for a second
    ledsOff;
    digitalWrite(5, HIGH);   // turn the LED off by making the voltage LOW
    delay(250);
    ledsOff;
    digitalWrite(4, HIGH);
    
    delay(250);
    ledsOff;
    digitalWrite(5, HIGH)
    
    delay(250);
    ledsOff;
    digitalWrite(3, HIGH);
    delay(250);
  }
} 
```

You can define any pin in the void set up and now we change the pinmode to 3,4 and 5 output and pinmode 2 to input.
That way when you press on the switch that is connected to pinmode 2,the leds are blinking one after another.
The voltage level we use are High (1) and Low (0).So when it's on High that means it's On and when it's on 
Low that means it's Off.The Delay time is the wait time or pause you get between the leds when they go On and Off.
If you succesfully code and upload it to the Arduino,you should see the leds blinking one after another.

![tag](../img/blink3.jpeg)

 **What to do**
+ Wire up the breadboard to Arduino's 5V and ground connection
+ Next you place 3 leds on the breadboard
+ Attach the cathode(short leg)of each leds to ground using a 220 ohm resistor
+ Connect the anode(long leg)to pin3,4 and 5

+ Next you place the Switch on the breadboard
+ Attach one side to the power and the other side to the digital pin2 on the Arduino
+ Add a 10K-ohm resistor from the ground to the switch pin that's connected to the Arduino
+ This way the pull-down resistor connects the pin to the ground when the switch is open,
  so it reads low when there is no voltage coming through the switch
 
![tag](../img/7thblink.png)

 **Procedural programming**

![tag](../img/4.jpeg)

```
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);  

}

void ledsOff(){
  digitalWrite(3,LOW); 
  digitalWrite(4,LOW);
  digitalWrite(5,LOW);
}
void runningLeds(){
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay(250);
  ledsOff();
  digitalWrite(4, HIGH);
  
  delay(250);
  ledsOff();
  digitalWrite(5, HIGH);
  
  delay(250);
  ledsOff();
  digitalWrite(3,HIGH);
  delay(250);
}
```















