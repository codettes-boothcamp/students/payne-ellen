## Project Managment

Project Managment

- Gitlab Clone

![tag](../img/clone.png)

- Github Desktop clone

![tag](../img/rep.png)

- Local file system

![tag](../img/local.png)

- Change made in notepad++

![tag](../img/note.png)

- Github desktop auto change detection + commit text

![tag](../img/change.png)

- Git commit

![tag](../img/enter.png)

- Git push

![tag](../img/commit.png)

- Gitlab change made

![tag](../img/about.png)











