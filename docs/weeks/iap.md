**Interface**

**Week 1**

**The Raspberry Pi** is a mini single-board computer that plugs
into  a computer monitor or TV,and uses the standard keyboard and mouse.It was
developed in the United Kingdom by the Raspberry Pi Foundation to promote teaching of
basic computer to science in schools and enveloping countries.This device also
have the ability to make it possible for people of all ages to explore computing
and to do programming in languages like Scratch and Python.

Here you have **the Raspberry Pi model 2**.This is how **the top** and **the bottom** looks like.

![tag](../img/rbpi2.png)

![tag](../img/rbbpi2.png)


**The Tools you need**
 + Raspberry Pi
 + Ethernet or Cross Cables
 + SD Card(16GB min)
 + Power Cable micro USB
 + Laptop ethernet slot

How to find your ipadress on your laptop,you can type **Command Prompt** on your search bar.
In **Command Prompt** you type **ipconfig** and then press enter.

![tag](../img/cp.png)

**Connect your Raspberry Pi**

In **Command Prompt** you check if your pi is connected by Typing in:Ping 192.168.1.168(your Raspberry Pi adress)
If that's the case you get to see the reply like this.

![tag](../img/cp1.png)

**To Setup The Basic Raspberry Pi**
+ Download Raspbian version (Raspbian Jessie if using rpi2/Raspbian Wheezy works with pi3)
+ Install Disk32Imager
+ Install Putty
+ Install Winscp
+ Install Notepad ++
+ Install Angry ip scanner

 Next you **Insert SD Card** in laptop

 Write **Raspbian Jessie** on SD Card by using **Win32 DiskImager**

![tag](../img/pi6.png)

 Next you select **Raspberry Pi** on SD Card and then you add
 an empty file called **ssh**.

![tag](../img/ssh.png)

Then you edit the file **cmdline.txt** on **Notepad ++**. 

![tag](../img/cmdl.png)

Next you type in your IP adress **192.168.1.168**.
You must Save the file and move/insert SD Card to that **IP adress**.

![tag](../img/ip1.png)

**Putty** 
 Is a free and open-source terminal emulator, serial console and network file transfer application.
 It supports several network protocols, including SCP, SSH, Telnet, rlogin, and raw socket connection.
 It can also connect to a serial port.

**WinSCP** 
 Is a free, open-source file-transfer application that uses File Transfer Protocol,
 Secure Shell File Transfer Protocol and Secure Copy Protocol for plain or secure file transfer.

Both(Putty and Winscp) are working together: In Putty you must command line to make folders and files.
And in Winscp you see which file or folder you make

Open **Winscp** by typing in your Ip adress (host name:192.168.1.168),
next you type in username:pi and his password (raspberry) and press on Login.

![tag](../img/wins1.png)

![tag](../img/wins2.png)

Open **Putty** and type in the **IP adress**,select **ssh**,give it
a name and press on **Save**.

![tag](../img/putty1.png)

![tag](../img/putty2.png)



What you do next is **Login**

Login as:**pi** (enter)
Password:**raspberry** (enter)
When it's open you type in:**sudo raspi-onfig**

![tag](../img/srconf.png)

**Configuration**

To navigate you use the **Up down left right** keys on your keyboard.

![tag](../img/conft.png)

**Week 2 / Day 1**

**Installing Nodejs and Python on your Raspberry Pi**

**What is Node.js?**
+ Node.js is an open source server environment
+ Node.js is free
+ Node.js runs on various platforms(Window,Linux,Unix,Mac OS X,etc.)
+ Node.js uses JavaScript on the server

**What Can Node.js Do?**

+ Node.js can generate dynamic page content
+ Node.js can create, open, read, write, delete, and close files on the server
+ Node.js can collect form data
+ Node.js can add, delete, modify data in your database

**What is a Node.js File?**

+ Node.js files contain tasks that will be executed on certain events
+ A typical event is someone trying to access a port on the server
+ Node.js files must be initiated on the server before having any effect
+ Node.js files have extension ".js"

**Express** is a web application framework for Node.js

**JavaScript**
+ JavaScript is the Programming Language for the Web and HTML
+ JavaScript can update and change both HTML and CSS
+ JavaScript can calculate, manipulate and validate data

**The Basic Linux Commands**
1. cd-navigate through directories
2. cd..go back to root directory
3. ls-list
4. dir-directories
5. mkdir-make directory
6. touch index.js-make text file
7. rm-remove files
8. rmdir-remove directories

![tag](../img/doc.png)

This is the **Node.js Folder Structure**

>**nodejs**
>>**projects**
>>>**project1**
>>>>1. index.js
>>>>2. **public**
>>>>>a. index.html
>>>>>>b. **css**
>>>>>>>style.css
>>>>3. **package.json**
                                 
We learn to program the **Nodejs Folder Structure**
in the **Raspberry Pi** this way.

![tag](../img/nodejsfs.png)

The following step is to make sure the Node,npm,Python and pip version you open in
Putty are the latest version.
                                
![tag](../img/check.png)                                        

**To install Nodejs**

 - curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs

All npm packages contain a file, usually in the project root, called **package.json**
This file holds various metadata relevant to the project and it's used to give information to npm 
that allows it to identify the project as well as handle the project's dependencies.
It can also contain other metadata such as a project description, the version of the project in a particular distribution,
license information,even configuration data - all of which can be vital to both npm and to the end users of the package.
The **package.json file** is normally located at the root directory of a **Node.js project**.

   **Bootstrapping a Node.js Project**
 - Execute the following command **npm init -y** in your **project1** folder to initialize your Node.js project 
   with default settings.

Then, under the project directory, create the entry point of the application, a file named **index.js**.

![tag](../img/indexjs.png)

   **Creating an npm script to run the application**
 + You'll use nodemon to monitor your project source code and automatically restart your Node.js server whenever it changes.

Install **nodemon** => npm **i -D nodemon**

![tag](../img/dnodem.png)

**Integrating the Express Web Framework with Node.js**

 + To use  the Express Framework you must install The command first and it's called **npm i express**.

![tag](../img/npmiexp.png)

First open **package.json**  and delete the **test** script and create a **dev** script into your command.

```
    {
      "name": "whatabyte-portal",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "scripts": {
        "dev": "nodemon ./index.js"
      },
      "keywords": [],
      "author": "",
      "license": "ISC",
      "devDependencies": {
        "nodemon": "^2.0.2"
      },
      "dependencies": {
        "express": "^4.17.1"
      }
    }
```

All npm packages contain a file, usually in the project root, called package.json.
This file holds various metadata relevant to the project.
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies.

Next open **index.js** and populate it with the following template that defines the core structure
of an Express application.

``` 
 // index.js
    
    /**
     * Required External Modules
     */
    
    /**
     * App Variables
     */
    
    /**
     *  App Configuration
     */
    
    /**
     * Routes Definitions
     */
    
    /**
     * Server Activation
     */
     
```
    
Then you **edit** the code
+ Now,under the **Required External Modules** section,import the **express** and **path** package.
+ Next,under the **App Variables** section you execute the default function,exported by the **express** in **app**
  and define the **port**,then your server will use to listen for requests.
  It's value is **process.env.PORT** or **8000** by default.
+ Next you go back at **index.js** and update the **App configuration** section and there you
  define the **public** folder,where you put everything(like any CSS and image files).
+ Finally,under the **Server Activation** section,start a server listening for incoming
  requests on **port** and to display a message to confirm it's listening.

**NOTE** : Under the **Routes Definitions** section we don't have to fill in anything that's why it's **blank**.

```
 // index.js
    
    /**
     * Required External Modules
     */
    
    const express = require("express");
    const path = require("path");
    
    /**
     * App Variables
     */
    
    const app = express();
    const port = process.env.PORT ||"8000";
    
    /**
     *  App Configuration
     */
    
    
    app.use(express.static(path.join(__dirname, "public")));
    
    /**
     * Routes Definitions
     */
    
    
    
    /**
     * Server Activation
     */
    
    app.listen(port, () => {
      console.log(`Listening to requests on http://localhost:${port}`);
    });
    
```    

Open your **Public** folder->your **index.html** file->add the **Basic HTML** code.

```
<html>
<body>
<h1>My website</h1>
</body>
</html>
```

Now Execute **dev** to test the script and run the app: **npm run dev**

![tag](../img/rdev.png)

Now you open your browser and type in: http://192.168.1.168:8000/ to see your webpage.
Your Website should look like this,if everything is correct.

![tag](../img/myweb1.png)

**NOTE** : You can edit your website using Javascript, HTML and CSS.

**Day 2**Python**

Is an interpreted, high-level, general-purpose programming language.
Its language constructs and object-oriented approach aim to help programmers write clear,
logical code for small and large-scale projects.

**Flask** 

Is a lightweight WSGI web application framework.
It is designed to make getting started quick and easy, with the ability to scale up to complex applications. 
It began as a simple wrapper around Werkzeug and Jinja and 
has become one of the most popular **Python** web application frameworks.

**HTML (Hypertext Markup Language)** 

Is the standard markup language for documents designed to be displayed in a web browser.

**CSS (Cascading Style Sheets)**

Is a style sheet language used for describing the presentation of a document written in a markup language like **HTML**.

  **To Install Python**

+ Install Python 2.7
+ Install pip
+ Install flask using sudo pip install flask

This is the **Python Folder Structure**.

>**Python**
>>1. Index.py
>>2. **Static**
>>>+  Img
>>3. **Templates**
>>>+  index.html

+ How to install the **Python Folder Structure** on your **Pi**

![tag](../img/pypro1.png)

![tag](../img/pysta.png)

+ Next you open the **index.py** and copy the code.

```
 from flask import Flask, render_template
    
    app = Flask(__name__)
    
    @app.route('/')
    def index():
        return render_template('index.html')
    if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0')
```

+ **@app.route('/')**: this determines the entry point; the / means the root of the website, so **http://127.0.0.1:5000/**
+ **def index()**: this is the name you give to the route; this one is called **index**, 
  because it’s the index (or home page) of the website.

+ Enter the command **python index.py**

![tag](../img/pyinpy.png)

+ Now open **index.html** and copy **Basic HTML**.

```
 <html>
    <body>
    <h1>My website</h1>
    </body>
    </html>
```

![tag](../img/web2.png)

**ARDUINO UNO SERIAL COMMUNICATION**

+ At first you build a simple circuit using one led on Tinkercad.

![tag](../img/circuit.png)

+ Now you build the simple circuit on your **Arduino using one led**.
+ Open your **Arduino** and test it out with the **Basic Blink**.
+ After testing it out,connect the **Arduino** to your **Raspberry Pi**.

![tag](../img/circuit1.png)

+ You can use this code to turn the led on.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

+ Open **Arduino**-> **File** -> **Example** -> **Communication** -> **PhysicalPixel**.

![tag](../img/ppixel.png)

+ Now compile and upload this code to the Arduino:

```
const int ledPin = 13; // the pin that the LED is attached to
int incomingByte;      // a variable to read incoming serial data into

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = Serial.read();
    // if it's a capital H (ASCII 72), turn on the LED:
    if (incomingByte == 'H') {
      digitalWrite(ledPin, HIGH);
    }
    // if it's an L (ASCII 76) turn off the LED:
    if (incomingByte == 'L') {
      digitalWrite(ledPin, LOW);
    })
  }
}
```

+ Create a **Pyhton** file
+ Give it any name with **extension .py**
+ And then **sudo pip install pyserial**

![tag](../img/sudopip.png)

+ Type **lsusb** to see a list of your **usb-ports** on which your Arduino UNO is connected.
+ **dmesg | grep tty** to see the serial port for your Raspberry Pi, if using Raspberry Pi 2 then **/dev/ttyAMA0**

+ Open your file and add this code

```
 import serial
    import time
    # Define the serial port and baud rate.
    # Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager
    
    ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
    def led_on_off():
        user_input = input("\n Type on / off / quit : ")
        if user_input =="on":
            print("LED is on...")
            time.sleep(0.1) 
            ser.write(b'H') 
            led_on_off()
        elif user_input =="off":
            print("LED is off...")
            time.sleep(0.1)
            ser.write(b'L')
            led_on_off()
        elif user_input =="quit" or user_input == "q":
            print("Program Exiting")
            time.sleep(0.1)
            ser.write(b'L')
            ser.close()
        else:
            print("Invalid input. Type on / off / quit.")
            led_on_off()
    
    time.sleep(2) # wait for the serial connection to initialize
    
    led_on_off()
```

+ Finally to make it work type **sudo python (name).py** (**e.g.** Mine is **spy.py** so **sudo python spy.py**)

![tag](../img/sudopy.png)

+ Now When you type **on** the Led will turn **On**

![tag](../img/ledon.png)

+ And if you type **off** the led will turn **Off**

![tag](../img/ledoff.png)

**HTLM/CSS/JavaScript**

**Day 1**

+ Learn to type code!
+ Learn HTML5/CSS3
+ Build your own basic website

![tag](../img/html.png)

**HTML or HyperText Mark-up Language** 

Is a standard markup language to produce visual output on a website or modern web-app. 
The HTML5 standard, the latest version, was released by the W3C Consortium in 2014.
W3C is a group that standardizes the world wide web languages and protocols to enable streamlined experience and 
integration between all browsers, and back-ends of participating members.

**CSS3 or Cascading Style Sheets**

Is a language that describes the style of an HTML document and how HTML elements should be displayed.
They enables the separation of the presentation and content,including layout,colors
and fonts.To improve content accessibility,more flexibility and control of presentation characteristics,
web pages share formatting by specifying the relevant CSS in a separate.css file.

+ The **3 languages** all web developers **must** learn are:

   1. **HTML** to define the content of web pages

   2. **CSS** to specify the layout of web pages

   3. **JavaScript** to program the behavior of web pages 
 
**Some Basic Tags**   

```
1. <B>FOR BOLD</B>
2. <H1> FOR HEADING1</H1>
3. <BR> FOR BREAK (go to the next line)
4. <img src="img/iot.jpg"/>
```

**And New Semantic Tags that actually have more meaning to the standard**

```
<DIV> tags for layout.
For layout : <article>, <header>, <footer>, <nav>, <aside> 
For widgets: <meter>, <progress> etc
```

**Steps before you create a website**

+1. Determine Message/Goal/Target audience (why the website)
+2. Make a site structure / site map of pages and sections of website

+3. Setup website folder structure

The root folder is **Public**

>**Public**
>>+ index.html
>>+  img
>>+   css
>>+    fonts
>>+    js
>>>>    lib


+4. **Collect content**

Once in the process of building a site it is best to have all content ready and available as much as possible.
It gives an idea of the amount of information for each webpage, sometimes forcing us to split it up or
at least give it a substructure to make it more readable.

+5. **Select the look & feel**

Get an idea up front on how you want to display your information

+6. **Build Mockups**

![tag](../img/mockup.png)

+7. **Target devices**

Decide what the target device(s) will be and if you want to support them all at once. 
This requires you to build Responsive site.










