- Second week
- Video production/learning how to install Adobe Premier

- All the steps of video editing after opening Adobe Premier

![tag](../img/first.png)

- Create a new project

![tag](../img/second.png)

- Give a name/capture Format-Select HDV

![tag](../img/thirdname.png)

- File-Import

![tag](../img/fourth.png)

- Editing working space
- To drag video to the timeline on right

![tag](../img/fifth.png)

- Where to select Effects

![tag](../img/sixth.png)

- Search for Ultra Key-Drag to right on video
- Effect control

![tag](../img/effectc.png)

- Change Key color/Remove background

![tag](../img/background.png)

- Import a background picture
- Drag between video and audio/scale for smaller or bigger

![tag](../img/impbackg.png)

- Add a Title

![tag](../img/3.png)

- Name it/Type a Title

![tag](../img/nametitle.png)

- Select any Text Font
- Select any color

![tag](../img/addingtitle.png)

- Drag Title between video and background
- stretch if necessary

![tag](../img/2.png)

- Export file

![tag](../img/export.png)

- Export settings
- format H.264

![tag](../img/expset.png)

- Select 'Preset'
- Youtube 720p HD

![tag](../img/preset.png)

- Output Name

![tag](../img/output.png)

- Save as

![tag](../img/output2.png)

- Click 'Export'

![tag](../img/export2.png)




