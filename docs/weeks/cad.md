**Tinkercad..3D Design.**

On Tinkercad you can design your own idea or sketch. All you need
is the tools that are given in Tinkercad. So be my guest and go wild
with your thoughts and hands.

**I made a design with a hole that ends by the bottom. And now i made a**
**duplicate of my design as a cover.**

![tag](../img/cad/1.png)

**Now i move the duplicate on top the original design. Click and drag**
**to move your item.**

![tag](../img/cad/2.png)

**Next i change my heights of my cover. This way you get the correct shape**
**of your cover.**

![tag](../img/cad/3.png)

**What i do next is putting the cover on the design.**

![tag](../img/cad/4.png)

**My last step is exporting my design to STL.I hope you find it helpful and interesting.**

![tag](../img/cad/5.png)


**Fusion 360..3D Design.**

**This is your worksheet and the tools you need in Fusion 360.**

![tag](../img/F.png)

To create a sketch you must first click on the square below the word Solid or 
you can just click on the word Create and next on Create Sketch.

![tag](../img/Fu1.png)

When you click on Create Sketch you can create a rectangle like i did
by change the height and thw width.Next you can create a circle in or out your 
rectangle.You click on Create--Circle--Center Diameter Circle c.

![tag](../img/F1.png)

Then you click on Modify -- Offset to create a offset and after this you click
on Finish Sketch to stop with sketching.

![tag](../img/F2.png)

This is your sketch.The next step here is to click on Extrude and then select
what you want to extrude.I select the wall of the rectangle push the arrow up.
I did the same with the circle.

![tag](../img/F3.png)
![tag](../img/F4.png)
![tag](../img/F7.png)


